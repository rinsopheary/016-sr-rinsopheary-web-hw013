import React, { Component } from "react";
import "./App.css";
import Menu from "./components/Menu";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import AddNewArticle from "./components/AddNewArticle";
import NotFound from "./components/NotFound";
import View from "./components/View";
import Edit from "./components/Edit";

export default class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Menu />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/add" component={AddNewArticle} />
            <Route path="/view/:id" component={View} />
            <Route path="/edit/:id" component={Edit} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}
