import React, { Component } from "react";
import { Button, Table } from "react-bootstrap";
import Axios from "axios";
import { Link } from "react-router-dom";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      data: [],
    };
  }

  fetchData = () => {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          loading: false,
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentDidMount() {
    this.fetchData();
  }

  handlerDelete(id) {
    console.log(id);

    Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(
      (rest) => {
        alert(rest.data.MESSAGE);
        this.fetchData();
      }
    );
  }

  getDate(date) {
    let y = date.substring(0, 4);
    let m = date.substring(4, 6);
    let d = date.substring(6, 8);
    return y + "-" + m + "-" + d;
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="container">
          <b>Data is loading ...</b>
        </div>
      );
    }

    let item = this.state.data.map((data) => (
      <tr key={data.ID} style={{ fontFamily: "Khmer OS Battambang" }}>
        <td>{data.ID}</td>
        <td>{data.TITLE}</td>
        <td>{data.DESCRIPTION}</td>
        <td style={{ width: 200 }}>{this.getDate(data.CREATED_DATE)}</td>
        <td>
          <img src={data.IMAGE} alt="" style={{ width: 100 }} />
        </td>
        <td style={{ width: 250, textAlign: "center" }}>
          <Link to={`/view/${data.ID}`}>
            <Button variant="primary">View</Button>
          </Link>
          &nbsp;
          <Link to={`/edit/${data.ID}`}>
            <Button variant="warning">Edit</Button>
          </Link>
          &nbsp;
          <Button variant="danger" onClick={() => this.handlerDelete(data.ID)}>
            Delete
          </Button>
        </td>
      </tr>
    ));
    return (
      <div className="container">
        <div className="row pt-5">
          <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
            <h1>Article Management</h1>
            <Link to="/add">
              <Button variant="dark">Add New Article</Button>
            </Link>
          </div>
        </div>

        <div className="row pt-4">
          <div className="col-lg-12 col-md-12 col-sm-12 col-12">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Created At</th>
                  <th>Images</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{item}</tbody>
            </Table>
          </div>
        </div>
      </div>
    );
  }
}
