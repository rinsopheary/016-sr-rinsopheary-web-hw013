import React, { Component } from "react";
import Axios from "axios";
import { Button, Form } from "react-bootstrap";

export default class Edit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      description: "",
      image: "",
      data: {},
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
          title: res.data.DATA.TITLE,
          image: res.data.DATA.IMAGE,
          description: res.data.DATA.DESCRIPTION,
        });
        console.log(res.data.DATA.TITLE);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit(e) {
    let id = this.props.match.params.id;
    let title = this.state.title;
    let description = this.state.description;
    let image = this.state.image;
    console.log(this.emptyTitle);

    if (title === "" && description === "") {
      alert("Please input field first");
    } else {
      let text = {
        TITLE: title,
        DESCRIPTION: description,
        IMAGE: image,
      };
      Axios.put(`http://110.74.194.124:15011/v1/api/articles/${id}`, text).then(
        (res) => {
          alert(res.data.MESSAGE);
          this.props.history.push("/");
        }
      );
    }

    this.setState({
      title: "",
      description: "",
    });
    e.preventDefault();
  }

  render() {
    let d = this.state.data;
    return (
      <div className="container">
        <div className="row pt-5">
          <h1>Update Article</h1>
        </div>
        <div className="row">
          <div className="col-lg-8 col-md-8 col-sm-8 col-8">
            <Form onSubmit={this.handleSubmit}>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  name="title"
                  value={this.state.title}
                  onChange={this.handleChange}
                />
                <span name="emptyTitle"></span>
              </Form.Group>

              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  name="description"
                  value={this.state.description}
                  onChange={this.handleChange}
                />
                {/* <span className="text-danger">Empty Text</span> */}
              </Form.Group>
              <Button variant="primary" type="submit">
                Save
              </Button>
            </Form>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-4 col-4">
            <img src={this.state.image} alt="" style={{ width: 300 }} />
          </div>
        </div>
      </div>
    );
  }
}
