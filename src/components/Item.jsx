// import React from "react";
// import { Button } from "react-bootstrap";
// import { Link } from "react-router-dom";
// import Axios from "axios";

// export default function Item({ data }, props) {
//   let id = data.ID;

//   function handlerDelete() {
//     Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(
//       (rest) => {
//         alert(rest.data.MESSAGE);
//       }
//     );
//   }

//   return (
//     <tr key={data.ID} style={{ fontFamily: "Khmer OS Battambang" }}>
//       <td>{data.ID}</td>
//       <td>{data.TITLE}</td>
//       <td>{data.DESCRIPTION}</td>
//       <td>{data.CREATED_DATE}</td>
//       <td>
//         <img src={data.IMAGE} alt="" style={{ width: 100 }} />
//       </td>
//       <td style={{ width: 250, textAlign: "center" }}>
//         <Link to={`/view/${data.ID}`}>
//           <Button variant="primary">View</Button>
//         </Link>
//         &nbsp;
//         <Button variant="warning">Edit</Button>&nbsp;
//         <Button variant="danger" onClick={() => handlerDelete()}>
//           Delete
//         </Button>
//       </td>
//     </tr>
//   );
// }
