import React, { Component } from "react";

export default class NotFound extends Component {
  render() {
    return (
      <div className="container pt-5 text-center">
        <b>ERROR 404 PAGE NOT FOUND</b>
      </div>
    );
  }
}
