import React, { Component } from "react";
import Axios from "axios";

export default class View extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
        // console.log(res.data.DATA);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    var d = this.state.data;
    return (
      <div className="container pt-5">
        <div className="row" style={{ fontFamily: "Khmer OS Battambang" }}>
          <div className="col-4">
            <h1>Article</h1>
            <img src={d.IMAGE} alt="" style={{ width: 300 }} />
          </div>
          <div className="col-8 pt-5">
            <b>
              <h4>{d.TITLE}</h4>
            </b>
            <p>{d.DESCRIPTION}</p>
          </div>
        </div>
      </div>
    );
  }
}
