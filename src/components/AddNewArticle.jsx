import React, { Component } from "react";
import { Button, Form } from "react-bootstrap";
import Axios from "axios";

export default class AddNewArticle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      description: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit(e) {
    let title = this.state.title;
    let description = this.state.description;

    if (title === "" && description === "") {
      alert("Please input field first");
    } else {
      let text = {
        TITLE: this.state.title,
        DESCRIPTION: this.state.description,
        IMAGE:
          "https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg",
      };
      Axios.post("http://110.74.194.124:15011/v1/api/articles", text).then(
        (res) => {
          alert(res.data.MESSAGE);
          this.props.history.push("/");
        }
      );
    }

    this.setState({
      title: "",
      description: "",
    });
    e.preventDefault();
  }

  render() {
    return (
      <div className="container">
        <div className="row pt-5">
          <h1>Add new Article</h1>
        </div>
        <div className="row">
          <div className="col-lg-8 col-md-8 col-sm-8 col-8">
            <Form onSubmit={this.handleSubmit}>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  name="title"
                  onChange={this.handleChange}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  name="description"
                  onChange={this.handleChange}
                />
              </Form.Group>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-4 col-4">
            <img
              src="https://omegamma.com.au/wp-content/uploads/2017/04/default-image.jpg"
              alt=""
              style={{ width: 300 }}
            />
          </div>
        </div>
      </div>
    );
  }
}
